(function(){

    'use strict'

    google.load( "feeds", "1" );

    var SELECTORS = {
        FEED_ID: 'feed',
        FEED_UL_CLASS: 'feed-list',
        FEED_LIST_ITEM_CLASS: 'feed-list__item',
        FEED_ANCHOR_CLASS: 'feed-list__item--link',
        FEED_DATE_CLASS: 'feed-list__item--date',
        FEED_CONTENT_CLASS: 'feed-list__item--content',

        MENU_BURGER_CLASS: 'main-menu__burger',
        MENU_HOLDER_CLASS: 'main-menu__holder right',

        MODAL_OPEN_CLASS: 'modal-open',
        MODAL_OVERLAY_CLASS: 'modal--overlay',
        MODAL_HEADER_TITLE_ID: 'modal__body--title',
        MODAL_HEADER_CLOSE_CLASS: 'modal__header--close',
        MODAL_BODY_CONTENT_ID: 'modal__body--content'
    };

    var feedContainer = document.getElementById( SELECTORS.FEED_ID );
    var bodyElement = document.getElementsByTagName("BODY")[0];
    var htmlElement = document.getElementsByTagName("HTML")[0];

    var FEED_URL = "http://news.google.com/?output=rss";
    var FEED_LIMIT = 10;
    var FEED_ON_PAGE = 3;
    var FEED_RELOAD_TIME = 60000;

    var feeds;

    function loadRssFeeds () {
        var feedpointer = new google.feeds.Feed( FEED_URL );
        feedpointer.setNumEntries( FEED_LIMIT );
        feedpointer.load( displayfeed );
    }

    function buildAnchorItemElement ( feed, idx ) {
        var anchor = document.createElement( 'A' );
        anchor.setAttribute( 'class', SELECTORS.FEED_ANCHOR_CLASS );
        anchor.setAttribute( 'href', '#' );
        anchor.setAttribute( 'data-id', idx );
        anchor.innerHTML = feed.title;

        return anchor;
    }
    function buildDateItemElement ( feed ) {
        var date = document.createElement( 'DIV' );
        date.setAttribute( 'class', SELECTORS.FEED_DATE_CLASS );
        date.innerHTML = feed.publishedDate;

        return date;
    }

    function buildContentElement ( feed ) {
        var content = document.createElement( 'DIV' );
        content.setAttribute( 'class', SELECTORS.FEED_CONTENT_CLASS );
        content.innerHTML = feed.contentSnippet;

        return content;
    }

    /*
     * Returns list of feed with titles
     */
    function buildFeedList ( feeds, offset, toShowCount ) {
        var ul = document.createElement( 'UL' );
        ul.setAttribute( 'class', SELECTORS.FEED_UL_CLASS );
        var toShowFeeds = feeds.slice( offset, offset + toShowCount );

        for ( var i = 0, max = toShowFeeds.length; i < max; i++ ) {
            var li = document.createElement( 'LI' );
            li.setAttribute( 'class', SELECTORS.FEED_LIST_ITEM_CLASS );
            li.appendChild( buildDateItemElement( toShowFeeds[ i ] ) );
            li.appendChild( buildAnchorItemElement( toShowFeeds[ i ], offset + i ) );
            li.appendChild( buildContentElement( toShowFeeds[ i ] ) );
            ul.appendChild( li );
        }

        return ul;
    }

    function clearElement ( htmlElement ) {
        var fc = htmlElement.firstChild;

        while ( fc ) {
            htmlElement.removeChild( fc );
            fc = htmlElement.firstChild;
        }
    }

    function displayfeed ( result ) {
        if (!result.error) {
            feeds = result.feed.entries;
            var offset = 0;

            var renderFeedList = function () {
                clearElement( feedContainer );
                feedContainer.appendChild( buildFeedList( feeds, offset, FEED_ON_PAGE ) );
                offset += 1;

                if (offset > FEED_LIMIT - offset) {
                    offset = 0;
                }

                setTimeout( renderFeedList, FEED_RELOAD_TIME );
            };
            setTimeout( renderFeedList, 0 );
        }
        else {
            console.error( "Error fetching feeds!" );
            feedContainer.style.display = "none";
        }
    }

    window.onload=function(){
        loadRssFeeds();
        bindEvents();
        initSliderJquery();
        showPage();
    }

    function showPage() {
        htmlElement.className += " open";
    }

    function initSliderJquery() {
        $('.banner-slider').bxSlider();
    }

    function toggleMenu ( e ) {
        var menuBlock = document.getElementsByClassName( SELECTORS.MENU_HOLDER_CLASS );
        var target = e.target || e.srcElement;
        if(target.className === SELECTORS.MENU_BURGER_CLASS) {
            menuBlock[ 0 ].className += " opened";
            target.className += " open";
            e.preventDefault();
        } else if(target.className === SELECTORS.MENU_BURGER_CLASS + ' open') {
            target.className = SELECTORS.MENU_BURGER_CLASS;
            menuBlock[ 0 ].className = SELECTORS.MENU_HOLDER_CLASS;
            e.preventDefault();
        }
    }

    function openModal ( e ) {
        var modalElem = document.getElementsByClassName( SELECTORS.MODAL_OVERLAY_CLASS );
        var titleElem = document.getElementById( SELECTORS.MODAL_HEADER_TITLE_ID );
        var contentElem = document.getElementById( SELECTORS.MODAL_BODY_CONTENT_ID );
        var target = e.target || e.srcElement;
        if (target.className === SELECTORS.FEED_ANCHOR_CLASS) {
            modalElem[ 0 ].className += " open";
            bodyElement.className = SELECTORS.MODAL_OPEN_CLASS;
            var id = target.getAttribute( "data-id" );
            titleElem.innerHTML = feeds[ id ].title;
            contentElem.innerHTML = feeds[ id ].content;
        }

        e.preventDefault();
    }

    function closeModal ( e ) {
        var modalElem = document.getElementsByClassName( SELECTORS.MODAL_OVERLAY_CLASS );
        var openModalClass = SELECTORS.MODAL_OVERLAY_CLASS + ' open';
        var target = e.target || e.srcElement;
        var hasCloseClass = target.className === SELECTORS.MODAL_HEADER_CLOSE_CLASS;
        var isModalClass = target.className === openModalClass;

        if (hasCloseClass || isModalClass) {
            modalElem[ 0 ].className = SELECTORS.MODAL_OVERLAY_CLASS;
            bodyElement.className = "";
            e.preventDefault();
        }

        e.stopPropagation();
    }

    function bindEvents () {
        var feed = document.getElementById( SELECTORS.FEED_ID );

        feed.addEventListener( 'click', openModal );
        document.addEventListener( 'click', closeModal );
        document.addEventListener( 'click', toggleMenu );
    }

})();