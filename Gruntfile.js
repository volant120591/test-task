module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            dist: {
                src: ['js/assets/outdatedBrowser.min.js', 'js/assets/jquery-1.11.1.min.js', 'js/assets/jquery.bxslider.js', 'js/main.js' ],
                dest: 'js/dest/build.js'
            },
            css: {
                src: ['css/assets/normalize.min.css', 'css/assets/outdatedBrowser.min.css', 'css/assets/reset.css', 'css/assets/jquery.bxslider.css', 'css/main.css'],
                dest: 'css/dest/build.css'
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    keepSpecialComments: 0
                },
                files: {
                    'dest/build.min.css': ['css/dest/build.css']
                }
            }
        },
        uglify: {
            build: {
                src: ['js/dest/build.js'],
                dest: 'dest/build.min.js'
            }
        },
        less: {
            development: {
                files: {
                    'css/main.css': 'less/main.less'
                }
            }
        },
        imagemin: {
            dynamic: {
                options: {
                    optimizationLevel: 7
                },
                files: [{
                    expand: true,
                    cwd: 'img/layout',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'dest/img'
                }]
            }
        },
        watch: {
            scripts: {
                files: ['js/*.js'],
                tasks: ['concat:dist', 'uglify'],
                options: {
                    spawn: false
                }
            },
            styles: {
                files: ['css/*.css'],
                tasks: ['concat:css', 'cssmin'],
                options: {
                    spawn: false
                }
            },
            less: {
                files: ['less/*.less'],
                tasks: ['less','concat:css', 'cssmin'],
                options: {
                    spawn: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat', 'cssmin', 'uglify']);
};